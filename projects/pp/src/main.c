/*
 * sensor_test.c
 *
 *  Created on: Aug 6, 2016
 *      Author: icv
 */

#include "board.h"
#include "main.h"
#include "bmp180.h"

static void initHardware(void);

void SysTick_Handler(void)
{
	if (pausems_count > 0) pausems_count--;
}

int main(void)
{
	int isOk;
	struct bmp180_sensor s;

	int p;
	float t;
	float a;

	initHardware();

	bmp180_init(&s, I2C0);
	if(bmp180_test(&s))
		return -1;

	//bmp180_init(&s, I2C0);
	bmp180_get_calibration_params(&s);
	bmp180_convert(&s);
	t = bmp180_get_temp(&s);
	p = bmp180_get_pressure(&s);
	bmp180_calculate_altitude(&s);
	a = bmp180_get_altitude(&s);

	return 0;
}


static void initHardware(void)
{
	   // Read clock settings and update SystemCoreClock variable
	   SystemCoreClockUpdate();
	   SysTick_Config(SystemCoreClock / 1000);
	   // Set up and initialize all required blocks and
	   // functions related to the board hardware
	   Board_Init();
	   // Set the LED to the state of "Off"
	   Board_LED_Set(0, false);

	   Board_I2C_Init(I2C0);
	   Chip_I2C_SetClockRate(I2C0, 100000);
	   Chip_I2C_SetMasterEventHandler(I2C0, Chip_I2C_EventHandlerPolling);
}
