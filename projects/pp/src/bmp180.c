/*
 * bmp180.c
 *
 *  Created on: Aug 9, 2016
 *      Author: icv
 */

#include "board.h"
#include "bmp180.h"
#include <math.h>

#define I2C_CLOCK_RATE	100000

/**
 * @brief: quickly implemented delay
 */
void bmp180_i2c_delay(void)
{
	for (int i = 0; i < 0xffff; i++);
}

/**
 * @brief: This function reads a register and returns the value
 *
 * @param: s: Sensor structure (proxy)
 * @param: addr: Register address to read. Refer to device memory map.
 * @param: out: Where to store register contents. Every register is 1 byte
 * long.
 *
 * @return: Zero if read was done. Not zero in other case.
 *
 */
int bmp180_read_register(struct bmp180_sensor *s, uint8_t addr, uint8_t *out)
{
	I2C_XFER_T xfer;
	uint8_t registerAddr;
	uint8_t data;
	int retval = 1;

	if (s->board_config.state != READY)
		return retval;

	registerAddr = addr;

	xfer.rxBuff = &data;
	xfer.rxSz = 1;
	xfer.slaveAddr = 0x77; /* As (oddly) described in datasheet */
	xfer.status = 0;
	xfer.txBuff = &registerAddr;
	xfer.txSz = 1;

	Chip_I2C_MasterTransfer(s->board_config.port, &xfer);

	bmp180_i2c_delay();
	*out = data;

	if (xfer.status == I2C_STATUS_DONE)
		retval = 0;

	return retval;
}

/**
 * @brief: This function writes a register. Every register is 1 byte long.
 *
 * @param: s: Sensor structure (proxy).
 * @param: addr: Register address.
 * @param: data: Data to write to register.
 *
 * @return: Zero if read was done. Not zero in other case.
 */
int bmp180_write_register(struct bmp180_sensor *s, uint8_t addr, uint8_t data)
{
	int retval = 1;
	I2C_XFER_T xfer;
	uint8_t txBuff[2];

	if (s->board_config.state != READY)
		return retval;

	txBuff[0] = addr;
	txBuff[1] = data;

	xfer.rxBuff = NULL;
	xfer.rxSz = 0;
	xfer.slaveAddr = 0x77; /* As described in datasheet */
	xfer.status = 0;
	xfer.txBuff = (const uint8_t *) txBuff;
	xfer.txSz = 2;

	Chip_I2C_MasterTransfer(s->board_config.port, &xfer);

	bmp180_i2c_delay();

	if (xfer.status == I2C_STATUS_DONE)
		retval = 0;

	return retval;
}

/**
 *
 * @brief: Reads a 16 bit register from device
 *
 * @param: s: Sensor structure (proxy).
 * @param: msbaddr: Address of MSB.
 *
 * @return: Register data.
 */
uint16_t bmp180_read_reg16(struct bmp180_sensor *s, uint8_t msbaddr)
{
	uint16_t retval;
	uint8_t  reg;

	bmp180_read_register(s, msbaddr, &reg);
	retval = ((uint16_t) reg) << 8;

	/* Every LSB address is MSB + 1 */
	bmp180_read_register(s, msbaddr + 1, &reg);
	retval |= reg;

	return retval;
}

/**
 *
 * @brief: Reads a 32 bit register from device
 *
 * @param: s: Sensor structure (proxy).
 * @param: msbaddr: Address of MSB.
 *
 * @return: Register data.
 */
uint32_t bmp180_read_reg32(struct bmp180_sensor *s, uint8_t msbaddr)
{
	uint32_t retval = 0;
	uint8_t  reg;

	bmp180_read_register(s, msbaddr, &reg);
	retval = ((uint32_t) reg) << 16;

	/* Every LSB address is MSB + 1 */
	bmp180_read_register(s, msbaddr + 1, &reg);
	retval |= ((uint32_t) reg) << 8;

	/* XLSB is MSB + 2 */
	bmp180_read_register(s, msbaddr + 2, &reg);
	retval |= reg;

	return retval;
}

/**
 *
 * @brief: Read and load all calibration data
 *
 * @param: s: Sensor structure (proxy).
 *
 */
void bmp180_get_calibration_params(struct bmp180_sensor *s)
{

	s->calibration.ac1 = bmp180_read_reg16(s, B180_REG_AC1_MSB);
	s->calibration.ac2 = bmp180_read_reg16(s, B180_REG_AC2_MSB);
	s->calibration.ac3 = bmp180_read_reg16(s, B180_REG_AC3_MSB);
	s->calibration.ac4 = bmp180_read_reg16(s, B180_REG_AC4_MSB);
	s->calibration.ac5 = bmp180_read_reg16(s, B180_REG_AC5_MSB);
	s->calibration.ac6 = bmp180_read_reg16(s, B180_REG_AC6_MSB);
	s->calibration.b1 = bmp180_read_reg16(s, B180_REG_B1_MSB);
	s->calibration.b2 = bmp180_read_reg16(s, B180_REG_B2_MSB);
	s->calibration.mb = bmp180_read_reg16(s, B180_REG_MB_MSB);
	s->calibration.mc = bmp180_read_reg16(s, B180_REG_MC_MSB);
	s->calibration.md = bmp180_read_reg16(s, B180_REG_MD_MSB);
}

/**
 * @brief: Checks if the sensor is performing a conversion.
 *
 * @param: s: Sensor structure (proxy).
 *
 * @return: Not zero if it's converting, zero if not.
 */
int bmp180_converting(struct bmp180_sensor *s)
{
	uint8_t ctrl_meas;
	int retval = 0;

	bmp180_read_register(s, B180_REG_CTRL_MEAS, &ctrl_meas);

	if (ctrl_meas & B180_SCO_MASK)
		retval = 1;

	return retval;
}

/**
 *
 * @brief: Just waits until conversion is done.
 *
 * @param: s: Sensor structure (proxy).
 *
 */
void bmp180_wait_conversion(struct bmp180_sensor *s)
{

	while(bmp180_converting(s))
		__WFI();
}

/**
 *
 * @brief: Convert a resolution (stored in bmp180_sensor structure) to
 * @brief: corresponding bits in OSS subregister of CTRL_MEASURE register
 *
 * @param: resolution: The desired resolution to convert.
 *
 * @return: A byte containing just the corresponding resolution bits on.
 */
uint8_t bmp180_resolution_to_oss(enum bmp180_res_modes_t resolution)
{
	uint8_t retval;

	switch(resolution)
	{
	default:
	case ULTRA_LOW_POWER_MODE:
		retval = B180_RES_ULP;
		break;
	case STANDARD_RESOLUTION_MODE:
		retval = B180_RES_STD;
		break;
	case HIGH_RESOLUTION_MODE:
		retval = B180_RES_HRES;
		break;
	case ULTRA_HIGH_RESOLUTION_MODE:
		retval = B180_RES_UHRES;
		break;
	case ADVANCED_RESOLUTION_MODE:
		retval = B180_RES_UHRES;
		/*
		 * TODO: Implement advanced resolution mode
		 */
		break;
	}

	return retval;
}
/**
 * @brief: Sets resolution in one of the available modes.
 * @brief: by default it's Ultra Low Power Mode.
 *
 * @param: s: Sensor structure (proxy).
 * @param: resolution: Resolution mode to set.
 *
 * Lower is faster. Advanced resolution mode is still not
 * implemented.
 *
 * @return: Zero if we could set the resolution, not zer oin other case.
 */
int bmp180_set_resolution(struct bmp180_sensor *s,
		enum bmp180_res_modes_t resolution)
{
	uint8_t res_bits;
	uint8_t ctrl_meas;
	int retval = 1;

	/* We won't change settings while performing a conversion */
	if (bmp180_converting(s))
		goto end;

	s->resolution = resolution;
	res_bits = bmp180_resolution_to_oss(resolution);

	if (bmp180_read_register(s, B180_REG_CTRL_MEAS, &ctrl_meas))
		goto end;

	//ctrl_meas &= ~B180_OSS_MASK;
	ctrl_meas = 0;
	ctrl_meas |= res_bits;
	ctrl_meas |= B180_CTRL_MEAS_PRES; /* From datasheet */

	if (bmp180_write_register(s, B180_REG_CTRL_MEAS, ctrl_meas))
		goto end;

	retval = 0;
end:
	return retval;
}

/**
 * @brief: Setter for resolution
 *
 * @param: s: Sensor structure (proxy).
 * @param: resolution: Desired resolution to configure.
 */
void bmp180_conf_resolution(struct bmp180_sensor *s,
		enum bmp180_res_modes_t resolution)
{

	s->resolution = resolution;
}

/**
 *
 * @brief: Read Uncompensated Temperature from device
 *
 * @param: s: Sensor structure (proxy).
 *
 * @return: 16 bit uncompensated temperature.
 */
int16_t bmp180_read_ut(struct bmp180_sensor *s)
{
	int16_t ut;

	ut = bmp180_read_reg16(s, B180_REG_OUT_MSB);

	return ut;
}

/**
 *
 * @brief: Read Uncompensated Pressure from device
 *
 * @param: s: Sensor structure (proxy).
 *
 * @return: 24 bit uncompensated temperature as 32 bit integer.
 */
int32_t bmp180_read_up(struct bmp180_sensor *s)
{
	int32_t up;
	uint8_t oss;

	up = bmp180_read_reg32(s, B180_REG_OUT_MSB);
	oss = bmp180_resolution_to_oss(s->resolution) >> B180_OSS_SHIFT;

	up >>= (8-oss); /* From datasheet */

	return up;
}

/**
 *
 * @brief: Compute temperature from uncompensated temperature as described
 * @brief: in datasheet.
 *
 * @param: s: Sensor structure (proxy).
 * @param: ut: Uncompensated temperature.
 */
void bmp180_compute_temp(struct bmp180_sensor *s, int16_t ut)
{

	int32_t X1, X2, B5, T;
	float temp;

	/* Black magic from datasheet */

	X1 = ((ut - s->calibration.ac6) * s->calibration.ac5) / pow2(15);
	X2 = (s->calibration.mc * pow2(11)) / (X1 + s->calibration.md);
	B5 = X1 + X2;
	s->calibration.b5 = B5;

	T = (B5 + 8) / pow2(4);

	/* T is in units of 0.1 C */

	temp = (float) T * 0.1;

	s->measurements.temp = temp;
}

/**
 *
 * @brief: Compute pressure from uncompensated pressureas described
 * @brief: in datasheet.
 *
 * @param: s: Sensor structure (proxy).
 * @param: up: Uncompensated pressure.
 */
void bmp180_compute_pressure(struct bmp180_sensor *s, uint32_t up)
{

	int32_t X1, X2, X3;
	int32_t B3, B6;
	int32_t p;
	uint32_t B4, B7;
	uint8_t oss;

	oss = bmp180_resolution_to_oss(s->resolution) >> B180_OSS_SHIFT;

	/* Black magic from datasheet */

	B6 = s->calibration.b5 - 4000;
	X1 = (B6 * B6) >> 12;
	X1 *= s->calibration.b2;
	X1 >>= 11;

	X2 = (s->calibration.ac2 * B6);
	X2 >>= 11;

	X3 = X1 + X2;
	B3 = (((((uint32_t)s->calibration.ac1) * 4 + X3) << oss) + 2) >> 2;

	X1 = (s->calibration.ac3 * B6) >> 13;
	X2 = (s->calibration.b1 * ((B6 * B6) >> 12)) >> 16;
	X3 = (X1 + X2 + 2) >> 2;

	B4 = (s->calibration.ac4 * (uint32_t)(X3 + 32768)) >> 15;
	B7 = ((uint32_t) (up - B3) * (50000 >> oss));



	if (B7 < 0x80000000) {
		if (B4 != 0)
			p = (B7 << 1) / B4;
	} else {
		if (B4 != 0)
			p = (B7 / B4) << 1;
	}

	X1 = p >> 8;
	X1 *= X1;
	X1 = (X1 * 3038) >> 16;

	X2 = (p * (-7357)) >> 16;

	p += (X1 + X2 + 3791) >> 4;

	s->measurements.pressure = p;
}

/**
 *
 * @brief: Calculate altitude from pressure
 *
 * @param: s: Sensor structure (proxy).
 *
 */
void bmp180_calculate_altitude(struct bmp180_sensor *s)
{

	float altitude;
	int32_t pressure;
	float c,ex;

	pressure = s->measurements.pressure;

	c = pressure / s->calibration.p0;
	ex = ((float) 1)/5.255;
	c = powf(c, ex);
	c = 1 - c;
	altitude = ((float) 44330) * c;
	altitude = 44330 * ((float)1-powf((float)pressure/(s->calibration.p0),(float)1/5.255));

	s->measurements.altitude = altitude;
}

/**
 *
 * @brief: Function to start a conversion. Both temperature and pressure.
 *
 * @param: s: Sensor structure (proxy).
 *
 * @return: Zero if operation was ok, not zero in other case.
 *
 * This function should block until a conversion of both temperature and
 * pressure is effectively done. Then it calculates the actual (physical)
 * values using calibration data.
 *
 * Using this physical data it computes altitude.
 *
 * It updates new_measure flag if it's properly done.
 */
int bmp180_convert(struct bmp180_sensor *s)
{
	uint16_t ut;
	uint32_t up;
	int retval = 1;

	if (bmp180_write_register(s, B180_REG_CTRL_MEAS, B180_CTRL_MEAS_TEMP))
		goto end;
	/* Maximum conversion time for temp is 4.5 ms */
	pausems(5);

	ut = bmp180_read_ut(s);

	if (bmp180_set_resolution(s, s->resolution))
		goto end;

	/* Maximum conversion time for pressure is 76.5 ms */
	pausems(77);

	up = bmp180_read_up(s);

	bmp180_compute_temp(s, ut);
	bmp180_compute_pressure(s, up);

	s->measurements.new_measure = 1;

	retval = 0;
end:
	return retval;
}

/**
 *
 * @brief: This function performs a software reset on the module
 *
 * @param: s: Sensor structure (proxy).
 *
 * @return: Zero if operation was ok, not zero in other case.
 *
 */
int bmp180_soft_reset(struct bmp180_sensor *s)
{
	int retval;

	/* 0xb6 as stated in datasheet */
	retval = bmp180_write_register(s, 0xe0, 0xb6);


	return retval;
}




/**
 *
 * @brief: Getter for temperature.
 *
 * @param: s: Sensor structure (proxy).
 *
 * @return: Temperature as read from the structure.
 */
float bmp180_get_temp(struct bmp180_sensor *s)
{

	s->measurements.new_measure = 0;
	return s->measurements.temp;
}

/**
 *
 * @brief: Getter for pressure.
 *
 * @param: s: Sensor structure (proxy).
 *
 * @return: Pressure as read from the structure.
 */
int32_t bmp180_get_pressure(struct bmp180_sensor *s)
{

	s->measurements.new_measure = 0;
	return s->measurements.pressure;
}

/**
 *
 * @brief: Setter for sea level pressure.
 *
 * @param: s: Sensor structure(proxy).
 * @param: p0: Pressure at sea level.
 *
 */
void bmp180_set_p0(struct bmp180_sensor *s, float p0)
{
	s->calibration.p0 = p0;
}

/**
 *
 * @brief: Getter for altitude.
 *
 * @param: s: Sensor structure (proxy).
 *
 * @return: Altitude as read from the structure.
 */
float bmp180_get_altitude(struct bmp180_sensor *s)
{

	s->measurements.new_measure = 0;
	return s->measurements.altitude;
}

int bmp180_new_measure_ready(struct bmp180_sensor *s)
{
	int retval;

	retval = s->measurements.new_measure;

	if (s->measurements.new_measure)
		s->measurements.new_measure = 0;

	return retval;
}

/**
 * @brief: Initializes necessary parts of hardware to work
 *
 * @param: s: Sensor structure (proxy)
 * @param: port: i2c port
 *
 * @return: Zero if everything went ok. Not zer in other case.
 *
 */
int bmp180_init(struct bmp180_sensor *s, I2C_ID_T port)
{
	int retval = 1;

	memset(s, 0, sizeof(struct bmp180_sensor));

	s->board_config.clkrate = DEFAULT_CLKRATE;
	s->board_config.port = port;
	s->board_config.state = READY;

	/* Default sea level pressure is 1013.25hPa */
	bmp180_set_p0(s, 101325);
	bmp180_get_calibration_params(s);

	if (bmp180_set_resolution(s, ULTRA_HIGH_RESOLUTION_MODE))
		goto end;

	retval = 0;

end:
	return retval;
}


/**
 * @brief: Test i2c connectivity. id register must be 0x55.
 *
 * @return: Zero if everything was ok. Not zero in other case.
 */
int bmp180_test(struct bmp180_sensor *s)
{
	int retval = 1;
	uint8_t id;

	bmp180_read_register(s, 0xd0, &id);

	if (id == 0x55)
		retval = 0;

	return retval;
}
