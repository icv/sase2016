/*
 * common.c
 *
 *  Created on: Aug 10, 2016
 *      Author: icv
 */


#include "common.h"
#include "board.h"

void pausems(uint32_t t)
{
	pausems_count = t;
	while (pausems_count != 0) {
		__WFI();
	}
}
