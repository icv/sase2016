/*
 * main.h
 *
 *  Created on: Aug 6, 2016
 *      Author: icv
 */

#ifndef _MAIN_H_
#define _MAIN_H_

#include <stdint.h>

int main(void);

#endif /* _MAIN_H_ */
