/*
 * common.h
 *
 *  Created on: Aug 10, 2016
 *      Author: icv
 */

#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdint.h>

uint32_t pausems_count;

void pausems(uint32_t t);


#endif /* _COMMON_H_ */
