/*
 * bmp180.h
 *
 *  Created on: Aug 6, 2016
 *      Author: icv
 */

#ifndef _BMP180_H_
#define _BMP180_H_

#define DEFAULT_CLKRATE 100000

#include "board.h"
#include <i2c_18xx_43xx.h>
#include <string.h>
#include "common.h"
// #include <stdint.h>

#define B180_REG_AC1_MSB 	0xaa
#define B180_REG_AC1_LSB 	0xab
#define B180_REG_AC2_MSB 	0xac
#define B180_REG_AC2_LSB 	0xad
#define B180_REG_AC3_MSB 	0xae
#define B180_REG_AC3_LSB 	0xaf
#define B180_REG_AC4_MSB 	0xb0
#define B180_REG_AC4_LSB 	0xb1
#define B180_REG_AC5_MSB 	0xb2
#define B180_REG_AC5_LSB 	0xb3
#define B180_REG_AC6_MSB 	0xb4
#define B180_REG_AC6_LSB 	0xb5
#define B180_REG_B1_MSB 	0xb6
#define B180_REG_B1_LSB 	0xb7
#define B180_REG_B2_MSB 	0xb8
#define B180_REG_B2_LSB 	0xb9
#define B180_REG_MB_MSB 	0xba
#define B180_REG_MB_LSB 	0xbb
#define B180_REG_MC_MSB 	0xbc
#define B180_REG_MC_LSB 	0xbd
#define B180_REG_MD_MSB 	0xbe
#define B180_REG_MD_LSB 	0xbf

#define B180_REG_ID 		0xd0
#define B180_REG_SOFT_RESET	0xe0
#define B180_REG_CTRL_MEAS	0xf4
#define B180_REG_OUT_MSB 	0xf6
#define B180_REG_OUT_LSB	0xf7
#define B180_REG_OUT_XLSB	0xf8

#define B180_CTRL_MEAS_TEMP	0x2e
#define B180_CTRL_MEAS_PRES 0x34

#define B180_OSS_MASK		0xc0
#define B180_SCO_MASK		0x20
#define B180_MEAS_CTRL_MASK	0x1f

#define B180_OSS_SHIFT		6
#define B180_RES_ULP		(0x00 << B180_OSS_SHIFT)
#define B180_RES_STD		(0x01 << B180_OSS_SHIFT)
#define B180_RES_HRES		(0x02 << B180_OSS_SHIFT)
#define B180_RES_UHRES		(0x03 << B180_OSS_SHIFT)

#define pow2(n) (1 << n)

enum bmp180_i2c_state_t {
	UNINITIALIZED = 0,
	READY,
	BUSY,
	UNKNOWN,
};

enum bmp180_res_modes_t {
	ULTRA_LOW_POWER_MODE = 0,
	STANDARD_RESOLUTION_MODE,
	HIGH_RESOLUTION_MODE,
	ULTRA_HIGH_RESOLUTION_MODE,
	ADVANCED_RESOLUTION_MODE
};

struct bmp180_sensor {

	enum bmp180_res_modes_t resolution;

	struct {
		float 		temp;
		int32_t 	pressure;
		float 		altitude;

		int   		new_measure;
	} measurements;

	struct {
		int16_t 	ac1;
		int16_t 	ac2;
		int16_t 	ac3;
		uint16_t 	ac4;
		uint16_t 	ac5;
		uint16_t 	ac6;
		int16_t 	b1;
		int16_t 	b2;
		int16_t 	mb;
		int16_t 	mc;
		int16_t 	md;

		int32_t		b5;
		float 		p0;

		int		state;
	} calibration;

	struct {
		uint32_t 				clkrate;
		I2C_ID_T 				port;
		enum bmp180_i2c_state_t	state;
	} board_config;
};

int bmp180_convert(struct bmp180_sensor *s);
int bmp180_soft_reset(struct bmp180_sensor *s);
int bmp180_set_resolution(struct bmp180_sensor *s,
		enum bmp180_res_modes_t resolution);
void bmp180_get_calibration_params(struct bmp180_sensor *s);
void bmp180_calculate_altitude(struct bmp180_sensor *s);

float bmp180_get_temp(struct bmp180_sensor *s);
int32_t bmp180_get_pressure(struct bmp180_sensor *s);
float bmp180_get_altitude(struct bmp180_sensor *s);
int bmp180_new_measure_ready(struct bmp180_sensor *s);

int bmp180_init(struct bmp180_sensor *s, I2C_ID_T port);
int bmp180_test(struct bmp180_sensor *s);

#endif /* _BMP180_H_ */
