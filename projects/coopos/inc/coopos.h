#include "stdint.h"

#define ALWAYS_RUN -1

void cooposInit(void);

void cooposHardwareInit(void);

void cooposRun(void);

void cooposAddTask(void (* taskPointer)(void),uint32_t period, uint32_t initialDelay,uint32_t howMany);
