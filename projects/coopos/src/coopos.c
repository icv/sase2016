#include "coopos.h"
//#include "chip.h"
#include "board.h"

#define TICKS_PER_SECOND 1000
#define MAX_NUM_TASKS 5
#define RUNNING 1
#define STOPPED 0

typedef struct
{
    void (* taskPointer)(void);
    uint8_t taskState;
    uint32_t period;
    uint32_t initialDelay;
    uint32_t howMany;

}taskStruct;

static taskStruct taskList[MAX_NUM_TASKS];
uint32_t ticks=0;

void cooposInit(void)
{
    uint8_t i=0;

    cooposHardwareInit();

    for (i=0;i<MAX_NUM_TASKS;i++)
    {
        taskList[i].taskPointer=NULL;
        taskList[i].taskState=STOPPED;
        taskList[i].period=100000;
        taskList[i].initialDelay=0;
        taskList[i].howMany=-1;
    }
}

void cooposHardwareInit(void)
{
  /* Config Core */
  SystemCoreClockUpdate();
  SysTick_Config(SystemCoreClock/TICKS_PER_SECOND);
}

void cooposRun (void)
{
      uint8_t i;

      for(i=0;i<MAX_NUM_TASKS;i++)
      {
        if(taskList[i].taskPointer!=NULL && taskList[i].taskState==RUNNING)
        {
          taskList[i].taskState=STOPPED;
          taskList[i].taskPointer();
        }
      }
      __WFI();

}

void cooposAddTask(void (* taskPointer)(void),uint32_t period, uint32_t initialDelay,uint32_t howMany)
{
      static uint8_t i=0;

      if(i<MAX_NUM_TASKS)
      {

            taskList[i].taskPointer=taskPointer;
            taskList[i].taskState=STOPPED;
            taskList[i].period=period;
            taskList[i].initialDelay=initialDelay;
            taskList[i].howMany=howMany;
      }

      i++;
}

void SysTick_Handler(void)
{
    uint8_t i;
    ticks++;

    for(i=0;i<MAX_NUM_TASKS;i++)
    {
      if(taskList[i].taskPointer!=NULL && ((ticks-taskList[i].initialDelay) % taskList[i].period)==0 && taskList[i].howMany!=0)
      {
               if(taskList[i].howMany>0)
               {
                    taskList[i].howMany--;
               }
               taskList[i].taskState=RUNNING;

      }
    }

}
